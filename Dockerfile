FROM node:13.8-alpine AS development

RUN apk --no-cache add --virtual builds-deps build-base python

WORKDIR /usr/app

COPY package*.json /usr/app

RUN npm install

EXPOSE 8080 
COPY . /usr/app
