// API
export default (req: any) => {
  try {
    const { walls } = req.body || [];
    const result = walls.map((item: any, index: number) => {
      let areaTotal = 0;
      const msgError = `Error na parede ${index + 1}: `;

      if (!validateArea(item.height, item.width)) {
        throw {
          status: 404,
          message:
            msgError + "Parede tem que ter mais de 1 m² e menos de 50 m².",
        };
      }

      if (
        !validateHeightWithDoor(item.width) &&
        item.quantityDoor &&
        item.quantitywindow
      ) {
        throw {
          status: 404,
          message:
            msgError +
            "Área das portas e janelas dever ser no máximo 50% da área total.",
        };
      }

      areaTotal = calculateAreaTotal(item.height, item.width);

      if (
        !validateDoorAndWindow(
          item.quantityDoor,
          item.quantityWindow,
          areaTotal
        )
      ) {
        throw {
          status: 404,
          message:
            msgError +
            "Altura da parede tem que ser no mínimo 30 centímetros maior que a altura da porta.",
        };
      }

      areaTotal -= calculateDoorAndWindow(
        item.quantityDoor,
        item.quantityWindow
      );

      const totalInk = calculateCanOfPaint(areaTotal);

      //return of the list in portuguese.
      return {
        parede: index + 1,
        total_de_litros: totalInk.totalLiters,
        lata_de_18_litros: totalInk.l18,
        lata_de_3_6_litros: totalInk.l306,
        lata_de_2_5_litros: totalInk.l205,
        lata_de_0_5_litros: totalInk.l005,
      };
    });

    return {
      status: 200,
      result,
    };
  } catch (err: any) {
    if (err.status != undefined) {
      return err;
    }
    return {
      status: 500,
      message:
        "Ocorreu um error na API, por favor verifique se o payload está correto.",
    };
  }
};

// ------------------------- AREA ----------------------------------------------- //

// function -> check if the total area less than 50m².
const validateArea = (height: number, width: number): boolean => {
  const areaTotal = width * height;
  return areaTotal >= 1 && areaTotal <= 50;
};

// Function -> area in m².
const calculateAreaTotal = (height: number, width: number): number => {
  return height * width;
};

// ------------------------- DOOR AND WINDOW ----------------------------------------------- //
const validateHeightWithDoor = (height: number): boolean => {
  return height < 1.9 + 0.3;
};

/**
 * @param qntDoor -> quantity door
 * @param qntWindow -> quantity Window
 * @param meters2Wall -> m² from the wall
 */

// Function -> check if the door and window has 50% less than total area.
const validateDoorAndWindow = (
  qntDoor: number,
  qntWindow: number,
  wall: number
): boolean => {
  return 0.8 * 1.2 * qntDoor + 2.0 * 1.2 * qntWindow <= wall / 2;
};

// Function  ->  calculate required area for door and window.
const calculateDoorAndWindow = (qntDoor: number, qntWindow: number) => {
  return 0.8 * 1.2 * qntDoor + 2.0 * 1.2 * qntWindow;
};

// ----------------------------- CAN OF PAINT ------------------------------------------- //

// Function -> calculate how many cans of paint do you need
const calculateCanOfPaint = (areaTotal: number): any => {
  let totalLiters = areaTotal / 5;
  const ink: any = {
    l18: 0,
    l306: 0,
    l205: 0,
    l005: 0,
    totalLiters: totalLiters,
  };

  while (totalLiters !== 0) {
    if (totalLiters < 0.5) {
      totalLiters = 0.5;
    }

    if (totalLiters >= 18 && totalLiters - 18 >= 0) {
      totalLiters -= 18;
      ink.l18++;
    }

    if (totalLiters < 18 && totalLiters - 3.6 >= 0) {
      totalLiters -= 3.6;
      ink.l306++;
    }

    if (totalLiters < 3.6 && totalLiters - 2.5 >= 0) {
      totalLiters -= 2.5;
      ink.l205++;
    }

    if (totalLiters < 2.5 && totalLiters - 0.5 >= 0) {
      totalLiters -= 0.5;
      ink.l005++;
    }
  }

  return ink;
};
