export default async (req: any, res: any, func: any) => {
  const resp = await func(req);
  res.status(resp.status).send(resp);
};
