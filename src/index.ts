import * as express from "express";
import calculateWallForInk from "./api/v1/calculate-wall-for-ink";
import responseWith from "./utils/response-with";
const app = express();
const PORT = process.env.PORT || 8080;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.post("/api/v1/ink", (req, res) => responseWith(req, res, calculateWallForInk));

app.listen(PORT, () => {
  console.log(`Server is open port: ${PORT}!!`);
});
