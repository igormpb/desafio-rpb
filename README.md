## Desafio Digital Republic!

## OBS
No desafio tive o problema com os números quebrados abaixo de 0.5, então decidi adicionar mais uma lata 0.5 litros para o usuário nessa situação.


## O que precisar fazer antes de rodar:

1º passo - Preciso ter docker na maquina.

2º passo - Apenas rodar o docker-compose up.

3º passo - Servidor estará disponivel na porta 8080.

## API

* **POST /api/v1/ink**

    Payload:

    ```
    {
    walls: [
        {
            "width": 4,
            "height": 4,
            "quantityDoor": 2,
            "quantityWindow": 1
        },
        {
            "width": 10,
            "height": 4,
            "quantityDoor": 0,
            "quantityWindow": 1
        },
         {
            "width": 4,
            "height": 3,
            "quantityDoor": 0,
            "quantityWindow": 1
        }
    ]
    }

    ```

Resposta:

```
   {
       {
       "status": 200,
       "result": [
           {
               "parede": 1,
               "total_de_litros": 7.136,
               "lata_de_18_litros": 0,
               "lata_de_3_6_litros": 1,
               "lata_de_2_5_litros": 1,
               "lata_de_0_5_litros": 3
           },
           {
               "parede": 2,
               "total_de_litros": 1.92,
               "lata_de_18_litros": 0,
               "lata_de_3_6_litros": 0,
               "lata_de_2_5_litros": 0,
               "lata_de_0_5_litros": 4
           }
       ]
   }
   }

```
